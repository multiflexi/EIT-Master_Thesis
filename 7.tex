\chapter{Product vision}
The vision of the product is to mainly have powerful local server/router/\gls{nas} with low power consumption and high security with customization which are possible thanks to openness of the device. Both open hardware and software are requirements for good security as anyone can explore the source code and find bugs. This is the \textit{gros} of Linus's Law which was formulated by Eric Steven Raymond in his book The Cathedral and the Bazaar: Musings on Linux and Open Source by an Accidental Revolutionary. It states \textit{``given enough eyeballs, all bugs are shallow''}. The possibility of customization follows the philosophy of Richard Stallman's concept of free software. Software is free as long as it follows these four rules\cite{free_soft}:
\begin{itemize}
\item Freedom 0: The freedom to run the program for any purpose.
\item Freedom 1: The freedom to study how the program works, and change it to make it do what you wish.
\item Freedom 2: The freedom to redistribute and make copies so you can help your neighbor.
\item Freedom 3: The freedom to improve the program, and release your improvements (and modified versions in general) to the public, so that the whole community benefits.
\end{itemize}

The technical concept revolves around high throughput networking and modularity. The router basic unit should include the network entry point and server. This is then complemented by multiple smaller local units which can offload some services from the main unit and provide multiple wired and wireless connection to the network. The \gls{cpu} architecture does not necessary have to be \gls{arm}. Quite contrary, better choice would be some of the open architectures like \gls{risc}-V which is sadly still not mature enough and the \gls{cpu} performance of the only currently produced \gls{sbc} SiFive HiFIve is somewhere between Raspberry Pi 2 and 3+.\cite{ricsv_phoronix}
 
The production plan would be for at least 10 years with minor revisions similarly to Raspberry Pi. The software support would be provided for all revisions with updates to newer kernels for same time. Used kernels would be only \gls{lts} versions. Devices would have to be part of mainline kernel.

The product vision got partially justified by introduction of modular open router Turris MOX described in part \ref{ch:mox} and the concept of using connection faster than \SI{1}{\giga\bit} in office and home environment and high performance \gls{wifi} was independently unveiled by Cisco, Asus, D-Link, Huawei H3C and is becoming a trend.\cite{cnet_dlink,asus_tomshw,huawei} 

The open hardware is gaining popularity and the examples are already mentioned \gls{risc}-V, Arduino, Raspberry Pi or RepRap project. The open source is now an industry standard and almost every major company is involved in open source development.
\section{Use cases}
Possible use cases for this solution are for example a audio studio recording music or dubbing, photo studio specializing in product photography and heavy high quality post-production, small to medium video capture or/and editing studio, security \acrshort{dvr} with \acrshort{uhd} cameras, scientific labs or any other offices with higher data throughput requirements. That could also be a home office which naturally shares network with all the members of family living there. We can imagine a situation when several members access network at the same time requiring low latency for gaming, fast downloading, streaming of high quality multimedia from local and/or external sources, remotely working with large files stored on local \gls{nas} and all this involving combination of wireless and wired clients. Another use case could be as a home server providing server oriented services, \gls{nas} for data storage with download clients and personal cloud, high performance router and living room multimedia computer when used with \gls{gpu}. Nowadays all these services are provided by separate devices.

Because the solution uses predominantly open source and free software it is suitable for public administration as they can customize it to fit their needs and have access to source code which is sometimes problematic in case of non-transparent behaviour of politicians. Administration can suffer from vendor lock-in and licence fees issues like in case of Czech Central Vehicle Registery and Prague's municipal smart card system Opencard. In the latter case the city managed to be vendor-locked-in because of disadvantageous contracts municipality made with software company eMoneyServices which developed the system. City was not owner of source code and was forced to use services of the eMoneyServices even when new administration wanted to make system transparent and cheaper. This costed taxpayers over \EUR{\num{6300000}} while new but still closed sourced but city owned system Lítačka costed around \EUR{\num{320000}}, approximately \SI{5}{\percent} of Opencard costs. Examples like this can be surely found in many other cities and countries. \cite{litacka_ihned}

The positive development towards open source in public administration happened two years ago in Bulgaria. New law was introduced according to which all newly developed software financed from public money has to have publicly accessible source code. The major part of law says:
\begin{quote}
\textit{``When the subject of the contract includes the development of computer programs, computer programs must meet the criteria for open-source software; all copyright and related rights on the relevant computer programs, their source code, the design of interfaces, and databases which are subject to the order should arise for the principal in full, without limitations in the use, modification, and distribution; and development should be done in the repository maintained by the agency in accordance with Art 7c pt. 18.''}
\end{quote}\cite{opensource_bulgaria}

\section{Competition}
\subsection{Traverse Five64 and LS1043S Router Board}
\begin{wrapfigure}{l}{0.5\textwidth}
	\vspace{-15pt}
	\begin{center}
		\includegraphics[width=0.48\textwidth]{./img/Five64.png}
	\end{center}
	\vspace{-10pt}
	\caption{Visualization of Five64}
	\vspace{-5pt}
\end{wrapfigure}
The most similar devices to MACCHIATObin are two boards from Australian company Traverse. Both boards are based on \gls{nxp} QorIQ LS1043 Cortex-A53 processor running at \SI{1.6}{\giga\hertz}. The main difference is that the Five64 board is not released and is now in state of preparation for \href{https://www.crowdsupply.com/traverse-technologies/five64}{Crowd Supply} campaign because it is an offshoot of LS1043S board. Compare to MACCHIATObin it uses less powerful \gls{cpu}, has only \SI{8}{\gibi\byte}, no \gls{sata} ports and only one \SI{10}{\giga\bit} \gls{sfp}+ port but it has 6 Gigabit Ethernet ports (one shares \gls{sfp} port similarly as Turris Omnia), two mini-\gls{pcie} slots and one M.2 slot for \gls{nvme} \gls{ssd}. It also has faster \gls{sd} card slot supporting \gls{uhs}-I and \gls{atx} front panel pins which MACCHIATObin lacks. Company promises support in mainline kernel since version 4.15 and newer but latest 4.17rc6 source code includes devicetree files only for the \gls{cpu} and none for board. From the company's \href{https://gitlab.com/traversetech}{GitLab} which includes LEDE and kernel code is also obvious that the \SI{10}{\giga\bit} interface is not yet working properly.

\subsection{GnuBee}
GnuBee is an open source \gls{nas} which is sold in two variants: PC-1 supports up to six \SI{2.5}{\inch} drives and PC-2 supports the same number of \SI{3.5}{\inch} drives. Otherwise almost identical devices are powered by dual core/quad thread \gls{mips} MediaTek MT7621A running at \SIrange[range-units = single]{880}{1200}{\mega\hertz} with \SI{512}{\mega\byte} \gls{ddr}3 memory, micro \gls{sd} or \gls{sd} card reader, two or three gigabit Ethernet ports, one \gls{usb} 3.0 and two \gls{usb} 2.0 ports. Advantage of larger model is that its two Ethernet ports can be bonded but even \SI{2}{\giga\bit} connection is quite limiting. The \gls{nas} can be used with Debian, OpenMediaVault, OpenWRT or libreCMC. Development of both devices was successfully financed through Crowd Supply campaigns. Model \href{https://www.crowdsupply.com/gnubee/personal-cloud-1}{Personal Cloud 1} raised over \SI{61000}[\$]{} with price of \SI{223}[\$]{} per unit and \href{https://www.crowdsupply.com/gnubee/personal-cloud-2}{Personal Cloud 2} over \SI{32000}[\$]{} with slightly higher price of \SI{285}[\$]{}. Main issue of device is that it is running old out-of-tree kernel version 3.10.14 or 4.4.87 and currently has limited support in mainline kernel which has to be patched before compilation. The networking and computational performance in comparison with MACCHIATObin is very low, there is only few Ethernet ports and therefore GnuBee is more suitable for document and multimedia storage at homes or maybe storage of low resolution feeds from security cameras.\cite{gnubee_campaign1,gnubee_campaign2,gnubee_homepage,gnubee_lwn}
\begin{figure}[H]
	\vspace{-5pt}
	\begin{center}
		\includegraphics[width=0.98\textwidth]{./img/gnubee.png}
	\end{center}
	\vspace{-15pt}
	\caption{GnuBee PC-2 circuit-board}
	\vspace{-5pt}
\end{figure}

\subsection{Odroid-HC1 and Odroid-HC2}
\begin{wrapfigure}{l}{0.5\textwidth}
	\vspace{-15pt}
	\begin{center}
		\includegraphics[width=0.48\textwidth]{./img/odroid.jpg}
	\end{center}
	\vspace{-10pt}
	\caption{Odroid-HC2 with drive bracket}
	\vspace{-5pt}
\end{wrapfigure}
Odroid-HC1 and Odroid-HC2 from Korean company Hardkernel are modifications of model ODROID-XU4, \gls{sbc} powered by octa core Samsung Exynos 5 Octa 5422 with quad core Cortex-A15 clocked at \SI{2.1}{\giga\hertz} and quad core Cortex-A7 \SI{1.4}{\giga\hertz}. The \gls{nas} variant has \SI{2}{\giga\byte} \gls{lpddr}3, one \gls{sata} 3 port, gigabit Ethernet, \gls{usb} 2.0 and \gls{uhs} micro-\gls{sd} card reader. Difference between models is as in GnuBee case the size of supported hard drives. Devices are shipped with Ubuntu 16.04.3 with current \gls{lts} kernel 4.14 and Debian, DietPi or OpenMediaVault. The unique but quite useless feature of this \gls{nas} oriented \gls{sbc} is Mali-T628 MP6 \gls{gpu} without video output which could be probably only used for accelerated video encoding. The units are stackable and therefore suitable when many small \glspl{nas} are needed. In comparison with MACCHIATObin this solution is very minimalistic and limited to only one hard drive, gigabit Ethernet and only client role in network. The prices of devices are \SI{58.50}[\$]{} and \SI{61}[\$]{}\footnote{Prices include power supply with power cord and cover.} excluding shipping.\cite{odroid}

\subsection{Helios4}
\begin{wrapfigure}{r}{0.5\textwidth}
	\vspace{-15pt}
	\begin{center}
		\includegraphics[width=0.48\textwidth]{./img/helios4.jpg}
	\end{center}
	\vspace{-10pt}
	\caption{Helios4 with maximum number of drives}
	\vspace{-5pt}
\end{wrapfigure}
Helios4 is open source \gls{nas} made by Singaporian company called Kobol. It is powered by dual core Cortex-A9 Marvell Armada 388 \gls{cpu} which is almost identical to the processor of Turris Omnia. \gls{sbc} has \SI{2}{\giga\byte} of \gls{ddr}3L with \gls{ecc}, four \gls{sata} 3.0 ports, gigabit \gls{lan}, two \gls{usb} 3.0 and micro\gls{sd} card reader. It is shiped with Debian 9, OpenMediaVault and is also able to run Free\gls{bsd}. Board is sold with case which can house up to four \SI{3.5}{\inch} hard drives which are cooled by two \SI{70}{\milli\meter} fans. The board was released at begining of 2018 after small campaign which sold over 300 units. The second campaign is in preparation with same price of \SI{195}[\$]{} shipping excluded. Compared to MACCHIATObin this device is limited by \SI[per-mode=symbol]{1}{\giga\bit\per\second} connection which in certain \gls{raid} configurations does hinder the performance of disk array even when \glspl{hdd} are used.\cite{kobol_wiki}
 

\subsection{Turris MOX}\label{ch:mox}
During the time of writing of this thesis CZ.NIC started an Indiegogo campaign to get funding for their new project Turris MOX. It is a modular open source router which uses similar hardware as Omnia and MACCHIATObin. Its basic modul A is based on dual core Cortex-A53 Marvell Armada 3720 running at \SI{1.2}{\giga\hertz}, it has \SIrange[range-units = single]{512}{1024}{\giga\byte} \gls{ddr}3 memory, gigabit \gls{wan}, \gls{usb} 3.0, \gls{sd} card slot and \gls{gpio} connector with \gls{sdio} interface for \gls{wifi}. It can be extended with six types of modules which can be connected through ``Moxtet'' system bus with \gls{pcie} 64-pin connector:
\begin{itemize}
	\item B - a mini-\gls{pcie} slot with \gls{sim} card for \gls{wifi}, \gls{lte}, \gls{sata}, \gls{nvme} \gls{ssd} (with M.2 adapter) or any other card. Only one module can be attached.
	\item C - Four Ethernet port switch with \gls{8p8c} connectors. Only one module can be attached and cannot be combined with module D because it terminates its \gls{sgmii} interface.
	\item D - \SI{2.5}{\giga\bit} \gls{sfp}+ connector which can either be used with \gls{dac} or with optical module. Only one module can be attached and cannot be combined with module C because it terminates its \gls{sgmii} interface.
	\item E - Eight Ethernet port switch with \gls{8p8c} connectors. Up to tree switch modules can be combined in one system because it is pass through.
	\item F - Module with four \gls{usb} 3.0. . Only one module can be attached and cannot be combined with module B.
	\item G - Pass through variant of module B. It can be combined with modules all other modules, therefore allowing to have up to two mini-\gls{pcie} slots or combination with module F.
\end{itemize}
It is also possible to buy three additional upgrades which add dual-band \gls{wifi}, more \gls{ram} or \gls{poe} functionality for module A. The ultimate configuration for this system would probably be a combination of modules G, B, D and three E modules with both upgrades. This would allow to have \SI{2.5}{\giga\bit} \gls{sfp}+ connection, twenty four switched gigabit ports plus one on module A and two mini-\gls{pcie} for additional expansion. This configuration would exceeded possibilities of Turris Omnia in number of \gls{lan} ports five times with cost of \SI{422}[\$]{}\footnote{A module with \gls{wifi}, \gls{ram} and \gls{poe} upgrades: $\SI{55}[\$]{}+3\times\SI{29}[\$]{}$, modules B and D: \SI{35}[\$]{} each, three modules E: $3\times\SI{55}[\$]{}$, module G: \SI{45}[\$]{}.} without tax and shipping.  

The first mention of MOX was cryptic slide at the end of Ondřej Filip's presentation ``DNSSEC Validation at CPE'' at \acrshort{icann}61 \printdate{2018-03-14}. The product was partially unveiled eleven days later and the campaign on Indiegogo started on \printdate{2018-04-03}. The goal of \SI{250000}[\$]{} was reached on \printdate{2018-05-16} and the campaign finished on \printdate{2018-06-1} with more than \SI{360000}[\$]{}. New modular Turris router runs on Turris OS forked from OpenWRT as Turris Omnia and it is already possible to boot other distributions like OpenSUSE. The plan is to bring support for MOX to upstream Linux kernel and OpenWRT.\cite{indiegogo_mox,mox_cz_nic,mox_docs,mox_lupa}
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.99\textwidth]{./img/mox.jpg}
	\end{center}
	\vspace{-10pt}
	\caption{Turris MOX modules A, B, C and D}
\end{figure}

\subsection{Banana Pi BPI-W2}
This \gls{sbc} from Chinese company Sinovoip is powered by quad-core \gls{arm}v8 Cortex-A53 Realtek RTD1296 \gls{cpu} running at \SI{1.5}{\giga\hertz}. It has \SI{2}{\giga\byte} \gls{ddr}4 \gls{ram} which is shared with Mali T820 MP3 \gls{gpu} with \gls{dsp} capable of decoding \gls{hdr} H.265 \SI{10}{\bit} at 60 \gls{fps} along with other video formats. The board can display this through mini\gls{dp} or \gls{hdmi} 2.0a output and also can record from its \gls{hdmi} 2.0a input but only in $1920\times1080$ resolution in H.264 standard. Storage options include up to \SI{64}{\giga\byte} \gls{emmc}, micro\gls{sd} card slot or two \gls{sata} 3.0 ports. The board can accommodate one mini-\gls{pcie} 2.0, one mini-\gls{pcie} 1.1 card with \gls{sdio} and one M.2 with \gls{sim} card slot for \gls{lte} modem. Wired connection includes two gigabit Ethernet interfaces and \gls{usb} 3.0 type A, two \gls{usb} 2.0 type A and also one type C. Board also has \gls{rtc}, infrared receiver and microphone input. The shipped software includes Android 7.1.1, Ubuntu 16.04 and OpenWRT but without support in mainline kernel.\cite{bpi,bpi_forum}
\begin{figure}[H]
	\begin{center}
		\includegraphics[width=0.99\textwidth]{./img/bpi-w2.jpg}
	\end{center}
	\vspace{-10pt}
	\caption{Banana Pi BPI-W2}
\end{figure}

\section{Improvements}
\subsection{Next Generation \acrshort{wifi}}
First obvious upgrade is move to \gls{ieee} 802.11ax \gls{wifi} standard which operates in both \SI{2.4}{\giga\hertz} and \SI{5}{\giga\hertz}. Thanks to more efficient utilization of spectrum the maximum data rate should increase by \SI{37}{\percent} to but the real life throughput should be up to four times higher. This is all thanks to \gls{ofdma} with tighter subcarrier spacing, longer symbol time and larger \gls{ofdm} \gls{fft} size, downlink and uplink \gls{mumimo}, 1024-\gls{qam} modulation and other improvements. The theoretical data rate should be \SI[per-mode=symbol]{287}{\mega\bit\per\second} for one \SI{40}{\mega\hertz} wide spatial stream in \SI{2.4}{\giga\hertz} band and \SI[per-mode=symbol]{1200}{\mega\bit\per\second} for one  \SI{160}{\mega\hertz} wide spatial stream in \SI{5}{\giga\hertz} band. Standard should also lower power consumption by sleeping time negotiation called \gls{twt}.

As of time of writing, several 802.11ax capable chips were introduced from companies like Marvell, Broadcom or Qualcomm and also several routers were introduced from companies like Asus, D-Link, H3C or Huawei. Majority of them does not implement full specification of standard, mainly missing the possibility to use eight spatial streams but some are using two radios in order to provide $2\times 4\times4:4$ modes in \SI{5}{\giga\hertz} band.

Another contemporary standard 802.11ad is usually used for high throughput point-to-point connections like wireless \gls{vr} or laptop docking. It operates in \SI{60}{\giga\hertz} band, has four \SI{2.16}{\giga\hertz} channels and provides maximal data rate of \SI[per-mode=symbol]{6756.75}{\mega\bit\per\second} per channel. This will be improved by new 802.11ay standard which will add channel bonding, $8\times8:8$ \gls{mumimo}, beamforming and and 256\gls{qam} to \SI[per-mode=symbol]{11000}{\mega\bit\per\second} per channel with single spatial stream.

This means that one device could theoretically achieve combined data rate of \SI[per-mode=symbol]{295.1}{\giga\bit\per\second}\footnote{24 channels with 8 spatial streams in \SI{5}{\giga\hertz} band and 4 non-overlapping channels in \SI{2.4}{\giga\hertz} band with 4 spatial streams, each channel \SI{20}{\mega\hertz} and 6 channels, each \SI{2160}{\mega\hertz} with 4 spatial streams in \SI{60}{\giga\hertz} band is approximately $24\times8\times\SI[per-mode=symbol]{150}{\mega\bit\per\second}+4\times4\times\SI[per-mode=symbol]{150}{\mega\bit\per\second}+6\times4\times\SI[per-mode=symbol]{11000}{\mega\bit\per\second}=\SI[per-mode=symbol]{295100}{\mega\bit\per\second}$}. This is just data rate of a theoretical device which is not achievable in real life conditions because of overheads, interference and also different local regulations - not all channels are available in all countries. If the promises of higher real throughput will become true, devices will have to move from gigabit Ethernet to \SI{2.5}{\giga\bit} in lower end, \SI{5}{\giga\bit} in mainstream or even \SI{10}{\giga\bit} and higher if several individual radios are deployed in order to use whole available spectrum with one device. It is imaginable that \gls{vr} and wireless displays will the key drivers for deployment of these new standards. This will put pressure not only on already mentioned Ethernet connections but also on internal interfaces and processing chips of routers and \glspl{ap}. For example in case of promised \SI[per-mode=symbol]{176}{\giga\bit\per\second} 802.11ay data rate we calculate with real throughput of $\SI{33}{\percent}=\SI[per-mode=symbol]{7.3}{\giga\byte\per\second}$, used \gls{wifi} adapter needs to be connected at least by \gls{pcie} $\times8$ 3.0 or $\times2$ 5.0 which is expected to come next year. This will be a challenge for manufacturers because recent \gls{wifi} adapters use only \gls{pcie} of second generation with single lane. On the other hand this is because there is no need for higher throughput. Necessity for multiple lines means switch to M.2 interface which can provide up to four lanes. For similar configuration in 802.11ax, \SI{160}{\mega\hertz} bonded channel with 8 spatial streams, is Turris Omnia with single line \gls{pcie} 2.0 still capable enough even if the efficiency rises to \SI{41}{\percent}.
\subsection{Multimedia playback}
The device could be also used as multimedia player if used with \gls{gpu}. The used full sized \gls{pcie} has great advantage for upgradeability. With new codecs being released, fully integrated solutions loose relevancy because they cannot decode them in hardware. Same applies for OpenGL and OpenCl. Hardware acceleration can be used not only for decoding but also for real-time transcoding and subsequent streaming over network to device which does not support original video coding standard. Currently the only graphic cards with available drivers for Aarch64 are those with AMD \glspl{gpu}.



